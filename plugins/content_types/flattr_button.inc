<?php

$plugin = array(
  'single' => TRUE,
  'title' => t('flattr button'),
  'description' => t('Shows a flattr button with basic configuration options.'),
  'category' => t('Social media'),
  'edit form' => 'flattr_pane_flattr_button_edit_form',
  'render callback' => 'flattr_pane_flattr_button_render',
  'admin info' => 'flattr_pane_flattr_button_admin_info',
  'defaults' => array(
    'username' => 'wiifm',
    'flattr_categories' => 5,
    'flattr_tags' => 5,
    'flattr_compact' => 'Compact',
  )
);

/**
 * 'admin info' callback for panel pane.
 */
function flattr_pane_flattr_button_admin_info($subtype, $conf, $contexts) {
  if (!empty($conf)) {
    $block = new stdClass;
    $block->title = $conf['override_title'] ? $conf['override_title_text'] : '';
    $block->content = t('Showing @flattr_categories from <em>@@username</em>.', array(
      '@flattr_categories' => $conf['flattr_categories'],
      '@username' => $conf['username'],
    ));
    return $block;
  }
}

/**
 * 'Edit form' callback for the content type.
 */
function flattr_pane_flattr_button_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['username'] = array(
    '#title' => t('Flattr username'),
    '#description' => t('The username of the flattr account.'),
    '#type' => 'textfield',
    '#default_value' => $conf['username'],
    '#required' => TRUE,
  );

  $form['flattr_categories'] = array(
    '#title' => t('Categories to show'),
    '#description' => t('Used to control the flattr categories shown on the page initially. Defaults to 5.'),
    '#type' => 'textfield',
    '#default_value' => $conf['flattr_categories'],
    '#required' => TRUE,
  );

  $form['flattr_tags'] = array(
    '#title' => t('tags to show'),
    '#description' => t('Used to control the flattr tags shown on the page initially. Defaults to tags.'),
    '#type' => 'textfield',
    '#default_value' => $conf['flattr_tags'],
    '#required' => FALSE,
  );
  $form['flattr_compact'] = array(
    '#title' => t('Select the compact or normal button size'),
    '#description' => t('The Flattr button comes in two sizes, the "normal", and the "compact". This is setting controls which button will be displayed'),
    '#type' => 'select',
    '#options' => array('Compact', 'Normal'),
    '#default_value' => $conf['flattr_compact'],
    '#required' => FALSE,
  );
  
#  $form['flattr_button'] => array(
#    '#title' => t('Select the compact or normal button size'),
##    '#description' => t('The Flattr button comes in two sizes, the "normal", and the "compact". This is setting controls which button will be displayed'),
#    '#type' => 'textfield',
#    '#default_value' => $conf['flattr_button'],
##    '#options' => array('Compact', 'Normal'),

#  );

  return $form;
}

/**
 * The submit form stores the data in $conf.
 */
function flattr_pane_flattr_button_edit_form_submit($form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}

/**
 * Run-time rendering of the body of the block (content type)
 * See ctools_plugin_examples for more advanced info
 */
function flattr_pane_flattr_button_render($subtype, $conf, $panel_args, $context = NULL) {
  $block = new stdClass();

  // initial content is blank
  $block->title = '';
  $block->content = '';

  $flattr_attributes = array(
    'uid' => $conf['username'],
    'category' => $conf['flattr_categories'],
    'tags' => $conf['flattr_tags'],
    'button' => ($conf['flattr_compact'] == 0) ? 'compact' : 'normal',
  );

  $sitename = variable_get('site_name');
  $attributes = array();
  $attributes['title'] = $sitename;
  $attributes['lang'] = array('de' => 'de_DE');
  $attributes['rel'] = 'flattr;' . _flattr_attributes($flattr_attributes);
  $attributes['class'][] = 'FlattrButton';
  $options['attributes'] = $attributes;
  $block->content .= l($sitename, $conf['username'], $options);
  drupal_add_js('//api.flattr.com/js/0.6/load.js?mode=auto', 'external');

  return $block;
}

/**
 * Flattr syntax version of drupal_attributes().
 *
 * @See drupal_attributes()
 */
function _flattr_attributes($attributes) {
  foreach ($attributes as $attribute => &$data) {
    $data = implode(',', (array) $data);
    $data = $attribute . ':' . check_plain($data) . ';';
  }
  return $attributes ? '' . implode('', $attributes) : '';
}

